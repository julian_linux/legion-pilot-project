'use strict';

const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin');

// const ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

const path = require('path');

const ENV = process.env.npm_lifecycle_event;

const isProd = ENV === 'build';

module.exports = (function makeWebpackConfig () {

  const config = {};

  config.entry = {
    app: './src/app/index.js',
    vendor: './src/app/vendor.js'
  };

  config.output = {
    path: path.resolve(__dirname, './dist'),
    publicPath: isProd ? 'Scripts/dist/' : 'http://localhost:8080/',
    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',
    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js'
  };

  if (isProd) {
    config.devtool = 'source-map';
  } else {
    config.devtool = 'eval-source-map';
  }

  config.resolve = {
    root: path.resolve(__dirname),
    modulesDirectories: [
      'node_modules',
      'src/'
    ],
    alias: {
      npm: 'node_modules',
      src: 'src',
      app: 'src/app',
      components: 'src/app/components',
      common: 'src/app/common',
      models: 'src/app/models',
      vendor: 'src/vendor',
      assets: 'src/assets'
    },
    extensions: ['', '.js'],
  };

  config.module = {
    preLoaders: [],
    loaders: [{
      test: /\.js$/,
      loaders: ['ng-annotate', 'babel'],
      exclude: /node_modules/
    }, {
      test: /\.scss$/,
      loaders: ["style", "css?sourceMap", "sass?sourceMap"]
    }, { 
      test: /\.css$/, 
      loader: 'style-loader!css-loader' 
    },  
    // {
    //   test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
    //   loader: 'file'
    // },
    {
      test: /\.svg$/,
      loader: 'url-loader?limit=100000&mimetype=image/svg+xml&name=images/[sha512:hash:base64:7].[ext]',
    }, {
      test: /\.gif$/,
      loader: 'url-loader?limit=100000&mimetype=image/gif&name=images/[sha512:hash:base64:7].[ext]',
    }, {
      test: /\.png$/,
      loader: 'url-loader?limit=100000&mimetype=image/png&name=images/[sha512:hash:base64:7].[ext]',
    },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader:"url?limit=10000&mimetype=application/font-woff" },
      { test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file" }, 
    {
      test: /\.html$/,
      loader: 'raw',
      exclude: /index\.html/
    }]
  };

  config.plugins = [];
  
  config.plugins.push(
    new HtmlWebpackPlugin({
      title: 'Pilot Project',
      template: './src/index.html',
      inject: 'body'
    })
  );

  config.plugins.push(
    new webpack.ProvidePlugin({
      // _: 'lodash',
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    })
  );

  // run if build phase
  if (isProd) {
    config.plugins.push(
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin(),
      new CopyWebpackPlugin([{
        from: path.resolve(__dirname, './src')
      }], { ignore: ['*.html'] })
    );
  }

  // set dev server for testing options
  config.devServer = {
    contentBase: './src',
    stats: 'minimal',
  };

  // return config object
  return config;
}());