import angular from 'angular'  
import uiRouter from 'angular-ui-router'
import angularMaterialize from 'angular-materialize';

import common from './common'
import components from './components'

import MainComponent from './main'
import * as mainConfig  from './main/main.config'

import { State } from './main/main.service';

const root = angular  
  .module('angularApp', [
    angularMaterialize,
    uiRouter, 
    common,
    components
  ])
  .component('app', MainComponent)
  .config(mainConfig.route)
  .config(mainConfig.httpProvider)
  .value('GLOBAL', mainConfig.global)
  .service('State', State)
  .name;

export default root;  
