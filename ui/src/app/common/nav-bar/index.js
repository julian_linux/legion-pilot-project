import angular from 'angular'  

import NavComponent  from './nav-bar.component'

export default angular  
  .module('app.common.nav-bar', [])
  .component('navBar', NavComponent)
  .name;
