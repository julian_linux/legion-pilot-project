import angular from 'angular'

import navBar from './nav-bar'
import headerBar from './header-bar'

export default angular
  .module('edr.common', [
    navBar,
    headerBar,
  ]).name