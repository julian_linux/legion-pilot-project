import angular from 'angular'  

import HeaderComponent from './header-bar.component'

export default angular  
  .module('app.common.header-bar', [])
  .component('headerBar', HeaderComponent)
  .name;
