export default class Punch {
    constructor() {
        this.accountId;
        this.userId;
        this.departmentId;
        this.id;
        this.paycodeId;
        this.inPunch;
        this.outPunch;
        this.benefitTime;
        this.editorId;
        this.editorName;
        this.note;
        this.date;
        this.action;
        this.lunchOther;
    }
}
