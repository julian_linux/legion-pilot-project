export const route = ($urlRouterProvider) => {
  'ngInject'
  $urlRouterProvider.otherwise('/');
}

export const httpProvider = ($httpProvider) => {
  'ngInject'
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
}

export const global = {
  requestParams: {
    url: 'http://pilotproject-001-site1.itempurl.com/',
    method: '',
    data: {},
    params: {},
    headers : {
      'Content-Type': 'application/json'
    }
  },
  punchTypes: [
    {id: 'reg', name: 'In/Out'},
    {id: 'break', name: 'Break'},
    {id: 'lunch', name: 'Lunch'},
    {id: 'benefit', name: 'Benefit'},
  ],
  payCodes: [
    { id: 1, name: 'Regular', code: 'REG'},
    { id: 2, name: 'Vacation', code: 'VAC'},
    { id: 3, name: 'Sick', code: 'SIC'},
    { id: 4, name: 'Holiday', code: 'HOL'},
    { id: 5, name: 'Other', code: 'OTH'},
    { id: 6, name: 'Break', code: 'BREAK'},
    { id: 7, name: 'Lunch', code: 'LUNCH'},
  ],
  benefitTypes: [
    { id: 'vac', name: 'Vacation (VAC)'}, 
    { id: 'hol', name: 'Holiday (HOL)'}, 
    { id: 'sic', name: 'Sick (SIC)'}, 
    { id: 'oth', name: 'Other (OTH)'},
  ]

}