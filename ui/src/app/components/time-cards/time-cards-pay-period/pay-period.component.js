import template from './pay-period.html';
import controller from './pay-period.controller';

export default {  
  template,
  controller,
  replace: true
};
