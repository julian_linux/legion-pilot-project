import angular from 'angular';

import component from './time-cards.component';
import config from './time-cards.config';
import service from './time-cards.service';

import modal from './time-cards-modal';
import payPeriod from './time-cards-pay-period';
import search from './time-cards-search';
import timeCard from './time-cards-time-card';

export default angular
    .module('app.components.time-cards', [
        modal,
        payPeriod,
        search,
        timeCard,
    ])
    .component('timeCards', component)
    .service('TimeCardsService', service)
    .config(config)
    .name;
