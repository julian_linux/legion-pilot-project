import template from './time-cards.html';
import controller from './time-cards.controller';

export default {  
  template,
  controller,
  replace: true,
};
