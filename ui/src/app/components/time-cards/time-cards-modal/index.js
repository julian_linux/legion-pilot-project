import angular from 'angular';

import component from './modal.component';

import punchesAdd from './punches-add';

import ModalService from './modal.service';

import './modal.scss';

export default angular
    .module('app.components.time-cards-modal', [
        punchesAdd
    ])
    .component('timeCardsModal', component)
    .service('ModalService', ModalService)
    .name;
