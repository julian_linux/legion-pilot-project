import R from 'ramda';
import moment from 'moment';

class PunchesAddController {
  constructor(ModalService, State, GLOBAL, $timeout){
    this.state = State;
    this.service = ModalService;
    this.global = GLOBAL;
    this.$timeout = $timeout;
  }

  enable(type){
    this._show[type]=!this._show[type];
  }

  select(type, id, value){
    this.data[id] = value;
    this.enable(type);

    if(type == 'punchTypes' && value == 'benefit'){
      this._show.benefits = true;
    }else if(type == 'benefitTypes'){
      this._show.benefits = true;
    }else{
      // this._show.benefits = false;
      // delete this.data.benefitType;
    }
  }

  show(type, id) {
    return R.filter(R.propEq('id', id), this[type])[0].name;
  }

  savePunch(e) {
    e.preventDefault();

    if(
      (!this.data.punchTypeId || !this.data.departmentId) 
      ||
      (this.data.punchTypeId !== 'benefit' && (!this.data.in || !this.data.out))
      ||
      (this.data.punchTypeId === 'benefit' && !this.data.benefitTypeId && !this.data[this.data.benefitTypeId])
    ){
      return;
    }

    const { user, selectedCard } = this.state.get();

    const payCodeId = this.data.punchTypeId === 'benefit' ? this.data.benefitTypeId : this.data.punchTypeId;
    const punch = {
      accountId: selectedCard.accountId,
      userId: selectedCard.userId,
      departmentId: this.data.departmentId,
      paycodeId: R.filter(R.propEq('code', R.toUpper(payCodeId)))(this.global.payCodes)[0].id,
      editorId: user.id,
      editorName: `${user.firstName} ${user.lastName}`,
      note: this.data.note || '',
      date: this.date.format('YYYY-MM-DD'),
      action: 1,
    };

    if (this.data.id) {
      punch.id = this.data.id;
      punch.action = 2;
    }

    if (!this.data[this.data.benefitTypeId]) {
      punch.inPunch = this.formatTime(this.data.in);
      punch.outPunch = this.formatTime(this.data.out);
    } else {
      punch.benefitTime = this.data[this.data.benefitTypeId];
    }

    this.service.savePunch(punch).then(id => {
      Materialize.toast('Punch Saved', 3000);
      this.parent.getPunchesByDate(this.index, true);
      this.parent.closePunchForm(this.index, this.parent.indexPunch);
    });
  }

  cancelPunch(e) {
    e.preventDefault();
    this.parent.closePunchForm(this.index, this.parent.indexPunch);
  }

  deletePunch() {
    const actualPunch = this.day.punches[this.parent.indexPunch];
    const punch = {};
    punch.id = actualPunch.id;
    this.service.deletePunch(punch).then(()=> this._delete());
  }

  _delete() {
    this.parent.days[this.index].punches.splice(this.parent.indexPunch, 1);
    this.$timeout(() => this.parent.closePunchForm(this.index, this.parent.indexPunch), 50);
  }

  formatTime(hour) {
    return this.date.format('YYYY-MM-DD')+' '+R.toUpper(hour);
  }

  configPunchEditInfo() {
    this.edditingPunch = true;
    const actualPunch = this.day.punches[this.parent.indexPunch];
    const { payCodes } = this.global;

    this.data.note = actualPunch.note;
    this.data.departmentId = actualPunch.departmentId;

    const punchTypeId = R.toLower(R.filter(R.propEq('id', actualPunch.paycodeId), payCodes)[0].code);

    if(actualPunch.paycodeId === 1) {

      this.data.punchTypeId = punchTypeId;
      this.data.in = actualPunch.inPunchTime;
      this.data.out = actualPunch.outPunchTime;

    } else if (R.contains(actualPunch.paycodeId, [2, 3, 4, 5]) ) {

      this.data.punchTypeId = 'benefit';
      this.data.benefitTypeId = punchTypeId;
      this._show.benefits = true;
      this.data[punchTypeId] = actualPunch.benefitTime;

    } else if (R.contains(actualPunch.paycodeId, [6, 7]) ) {

      this.data.punchTypeId = punchTypeId;
      this.data.reg = actualPunch.benefitTime;

    }

  }

  $onInit() {
    this.data = {};
    this._show = {
      punchTypes: false,
      departments: false,
      benefits: false,
      benefitType: false
    }
    const { user } = this.state.get();
    this.departments =user.departments;
    this.punchTypes = this.global.punchTypes;
    this.benefitTypes = this.global.benefitTypes;
    this.day = this.parent.days[this.index];
    this.date = this.day.date.date;
    !R.isNil(this.parent.indexPunch) && this.configPunchEditInfo();
  }
}

PunchesAddController.$inject = ['ModalService', 'State', 'GLOBAL', '$timeout'];

export default PunchesAddController;