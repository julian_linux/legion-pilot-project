export default ($stateProvider) => {
  'ngInject'
  $stateProvider
    .state('timeCards', {
      url: '/',
      component: 'timeCards'
    })
}
