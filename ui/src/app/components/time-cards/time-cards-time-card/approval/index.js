import angular from 'angular';

import component from './approval.component';

export default angular
    .module('app.components.time-card.approval', [])
    .component('approval', component)
    .name;
