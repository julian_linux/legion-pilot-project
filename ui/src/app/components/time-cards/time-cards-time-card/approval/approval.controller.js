import R from 'ramda';

class ApprovalController {
  $onInit() {
    this.showAdminBox = false;
    this.showSupervisorBox = false;
    this.showEmployeeBox = false;
    this.boxesClasses = {
      1: 'l12',
      2: 'l6',
      3: 'l4'
    };
    this.firstApproval = false;
    this.secondApproval = false;
    this.boxClass = '';
    this.checkStatus();
  }

  checkStatus(){
    const c = this.card;
    if(!c.accountTcRequireApproval)
      return;
    this.showAdminBox = c.accountTcRequireAdminApproval;
    this.showSupervisorBox = c.accountTcRequireSupervisorApproval;
    this.showEmployeeBox = c.accountTcRequireEmployeeApproval;
    this.countBoxes = (+this.showAdminBox) + (+this.showSupervisorBox) + (+this.showEmployeeBox);
    this.boxClass = this.boxesClasses[this.countBoxes];
    if (this.countBoxes == 1) {
      this.imgClass = 'one-image-circle-grey';

    }else{
      this.imgClass = 'image-circle-gray-small';
    }

  }
}

export default ApprovalController;