import template from './time-card.html';
import controller from './time-card.controller';

export default {  
  template,
  controller,
  replace: true,
  bindings: {
    card: '<',
    cardIndex: '<'
  },
  require: {
    parent: '^^timeCards'
  }
};
