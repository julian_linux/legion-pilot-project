import angular from 'angular';

import component from './search.component';

export default angular
    .module('app.components.time-cards.search', [])
    .component('search', component)
    .name;
