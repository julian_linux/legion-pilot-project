
import angular from 'angular'

import timeCards from './time-cards';

const components = angular
  .module('app.components', [
    timeCards,
  ])
  .name;

export default components; 