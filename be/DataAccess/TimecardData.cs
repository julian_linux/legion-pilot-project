﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace DataAccess
{
    public class TimecardData
    {
        private string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TimecardConn"].ConnectionString;

        #region Timecards
        /// <summary>
        /// Obtiene listado de timecards
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns></returns>
        /*
        public IList<TimecardModel> GetTimecard(TimecardModel timecard)
        {
            IList<TimecardModel> ret = new List<TimecardModel>();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_get_timecard", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = timecard.accountId;
                    command.Parameters.Add("_user_id", MySqlDbType.Int16).Value = timecard.userId;
                    command.Parameters.Add("_period_id", MySqlDbType.VarChar).Value = timecard.periodId;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while (iReader.Read())
                        {
                            ret.Add(MapSingleTimeCard(reader));
                        }
                    }
                }
            }

            return ret;
        }
        */
        /// <summary>
        /// Gets timecards
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public IList<TimecardModel> GetTimecardsPayroll(TimecardModel pTimecard)
        {
            IList<TimecardModel> ret = new List<TimecardModel>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_list_timecards_with_pay_codes", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pTimecard.accountId;
                    command.Parameters.Add("_admin_id", MySqlDbType.Int64).Value = null;
                    command.Parameters.Add("_period_id", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_keyword", MySqlDbType.VarChar).Value = null;
                    command.Parameters.Add("_admin_appoval", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_supervisor_approval", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_employee_approval", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_approval_disabled", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_order_by", MySqlDbType.Int16).Value = null;
                    command.Parameters.Add("_sort_dir", MySqlDbType.Int16).Value = null;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while(iReader.Read())
                        {
                            ret.Add(MapTimeCard(reader));
                        }
                    }
                }

            }

            return ret;
        }


        /// <summary>
        /// Edit time card
        /// </summary>
        /// <param name="pTimecard"></param>
        public void EditTimecard(TimecardModel pTimecard)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_up_user_timecard", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int16).Value = pTimecard.accountId;
                    command.Parameters.Add("_user_id", MySqlDbType.Int32).Value = pTimecard.userId;
                    command.Parameters.Add("_is_timecard_viewable", MySqlDbType.Bit).Value = pTimecard.isTimecardViewable;

                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion Timecards

        #region Users-Employees
        /// <summary>
        /// Get emplyees list
        /// </summary>
        /// <returns></returns>
        public IList<UserModel> GetEmployees()
        {
            IList<UserModel> employee = new List<UserModel>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_get_employee_details_20", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while (iReader.Read())
                        {
                            employee.Add(MapEmployee(reader));
                        }
                    }
                }
            }

            return employee;
        }

        /// <summary>
        /// Gets a user from accountid and userid
        /// </summary>
        /// <param name="pUser"></param>
        /// <returns></returns>
        public UserModel GetUser(UserModel pUser)
        {
            UserModel retUser = new UserModel();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pUser.accountId;
                    command.Parameters.Add("_user_id", MySqlDbType.Int32).Value = pUser.id;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while(reader.Read())
                        {
                            retUser = MapEmployee(reader);
                        }
                    }
                }
            }

            return retUser;
        }

        /// <summary>
        /// Search employees
        /// </summary>
        /// <param name="pUser"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public IList<UserModel> SearchEmployees(UserModel pUser, string keyword)
        {
            IList<UserModel> listUser = new List<UserModel>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_search_users", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pUser.accountId;
                    command.Parameters.Add("_role_id", MySqlDbType.Int32).Value = pUser.roleId;
                    command.Parameters.Add("_department_id", MySqlDbType.VarChar).Value = pUser.departmentId.ToString();
                    command.Parameters.Add("_admin_id", MySqlDbType.Int32).Value = pUser.id;
                    command.Parameters.Add("_keyword", MySqlDbType.VarChar).Value = keyword;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while(reader.Read())
                        {
                            listUser.Add(MapUsersSearch(reader));
                        }
                    }
                }
            }

            return listUser;
        }
        #endregion Users-Employees


        public IList<SummaryModel> GetHoursByDepartment(int accountId)
        {
            IList<SummaryModel> ret = new List<SummaryModel>();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_get_hours_by_department_by_paycode", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = accountId;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while(iReader.Read())
                        {
                            ret.Add(MapSummary(iReader));
                        }
                    }                        
                }
            }
            return ret;
        }


        /// <summary>
        /// Get summaries
        /// </summary>
        /// <param name="pSol"></param>
        /// <returns></returns>
        public IList<SummaryModel> GetSummaries(TimecardModel pSol)
        {
            IList<SummaryModel> sum = new List<SummaryModel>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_get_summaries", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int16).Value = pSol.accountId;
                    command.Parameters.Add("_department_id", MySqlDbType.Int32).Value = pSol.departmentId;
                    command.Parameters.Add("_timezone_id", MySqlDbType.Int32).Value = pSol.timeZoneId;
                    command.Parameters.Add("_period_id", MySqlDbType.Int32).Value = pSol.periodId;
                    command.Parameters.Add("_minute_format_id", MySqlDbType.Int32).Value = pSol.minuteFormatId;
                    command.Parameters.Add("_admin_id", MySqlDbType.Int32).Value = pSol.adminId;

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while(reader.Read())
                        {
                            sum.Add(MapSummary(reader));
                        }
                    }
                }
            }
            return sum;
        }

        #region Punches
        public IList<PunchModel> GetPunchesByDate(PunchModel pSol)
        {
            IList<PunchModel> punches = new List<PunchModel>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_get_punches_by_date", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pSol.accountId;
                    command.Parameters.Add("_user_id", MySqlDbType.Int32).Value = pSol.userId;
                    command.Parameters.Add("_date", MySqlDbType.DateTime).Value = pSol.date;
                    
                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        IDataReader iReader = reader;
                        while (reader.Read())
                        {
                            punches.Add(MapPunches(reader));
                        }
                    }
                }
            }
            return punches;
        }

        /// <summary>
        /// Add 
        /// </summary>
        /// <param name="pPunch"></param>
        public Int64 AddPunch(PunchModel pPunch)
        {
            // Int64 punch = new Int64();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand("sp_post_manual_punch", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pPunch.accountId;
                    command.Parameters.Add("_user_id", MySqlDbType.Int64).Value = pPunch.userId;
                    command.Parameters.Add("_department_id", MySqlDbType.Int64).Value = pPunch.departmentId;
                    command.Parameters.Add("_punch_id", MySqlDbType.Int64).Value = pPunch.id;
                    command.Parameters.Add("_paycode_id", MySqlDbType.Int64).Value = pPunch.paycodeId;

                    command.Parameters.Add("_in_punch", MySqlDbType.DateTime).Value = pPunch.inPunch;
                    command.Parameters.Add("_out_punch", MySqlDbType.DateTime).Value = pPunch.outPunch;

                    command.Parameters.Add("_benefit_time", MySqlDbType.Decimal).Value = pPunch.benefitTime;
                    command.Parameters.Add("_editor_id", MySqlDbType.Int32).Value = pPunch.editorId;
                    command.Parameters.Add("_editor_name", MySqlDbType.VarChar).Value = pPunch.editorName;
                    command.Parameters.Add("_note", MySqlDbType.VarChar).Value = pPunch.note;

                    command.Parameters.Add("_network", MySqlDbType.VarChar).Value = "192.168.0.1";

                    command.Parameters.Add("_date", MySqlDbType.Date).Value = pPunch.date;
                    command.Parameters.Add("_action", MySqlDbType.Int32).Value = pPunch.action;
                    command.Parameters.Add("_lunch_other", MySqlDbType.Int32).Value = pPunch.lunchOther;

                
                }
            }
            // how to get las punch id?. the sp dont return it
            return 1;
        }

        public void DeletePunch(PunchModel pPunch)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand("sp_del_punch", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("_account_id", MySqlDbType.Int32).Value = pPunch.accountId;
                    command.Parameters.Add("_punch_id", MySqlDbType.Int32).Value = pPunch.id;

                    command.ExecuteNonQuery();
                }
            }
        }
        #endregion Punches

        #region Maps
 
        private SummaryModel MapSummary(IDataReader rawData)
        {
            SummaryModel dep = new SummaryModel();

            dep.deparmentId = rawData["department_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["department_id"]);
            dep.deparmentName = rawData["department_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["department_name"]);
            dep.departmentCode = rawData["department_code"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["department_code"]);
            dep.endDate = rawData["end_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["end_date"]);
            dep.hol = rawData["hol"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["hol"]);
            dep.ot1 = rawData["ot1"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["ot1"]);
            dep.ot2 = rawData["ot2"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["ot2"]);
            dep.oth = rawData["oth"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["oth"]);
            dep.reg = rawData["reg"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["reg"]);
            dep.sic = rawData["sic"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["sic"]);
            dep.vac = rawData["vac"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["vac"]);
            dep.tot = rawData["tot"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["tot"]);
            dep.regp = rawData["regp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["regp"]);
            dep.ot1p = rawData["ot1p"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["ot1p"]);
            dep.ot2p = rawData["ot2p"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["ot2p"]);
            dep.vacp = rawData["vacp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["vacp"]);
            dep.holp = rawData["holp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["holp"]);
            dep.sicp = rawData["sicp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["sicp"]);
            dep.othp = rawData["othp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["othp"]);
            dep.totp = rawData["totp"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["totp"]);

            return dep;
        }

        private UserModel MapEmployee(IDataReader rawData)
        {
            UserModel model = new UserModel();
            model.accountId = rawData["account_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["account_id"]);
            model.masterUserId = rawData["master_user_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["master_user_id"]);
            model.roleId = rawData["role_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["role_id"]);
            model.rightId = rawData["right_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["right_id"]);
            model.departmentId = rawData["department_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["department_id"]);
            model.timeZoneId = rawData["timezone_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["timezone_id"]);
            model.email = rawData["email"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["email"]);
            model.firstName = rawData["first_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["first_name"]);
            model.lastName = rawData["last_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["last_name"]);
            model.username = rawData["username"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["username"]);
            model.password = rawData["password"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["password"]);
            model.phone = rawData["phone"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["phone"]);
            model.baseRate = rawData["base_rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rawData["base_rate"]);
            model.weeklyRule = rawData["weekly_rule"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["weekly_rule"]);
            model.payrollNumber = rawData["payroll_number"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["payroll_number"]);

            return model;

        }

        private PunchModel MapPunches(IDataReader rawData)
        {
            PunchModel modelo = new PunchModel();

            modelo.id = rawData["id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["id"]);
            modelo.userId = rawData["user_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["user_id"]);
            modelo.departmentId = rawData["department_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["department_id"]);
            modelo.departmentCode = rawData["department_code"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["department_code"]);
            modelo.paycodeId = rawData["paycode_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["paycode_id"]);
            modelo.punchType = rawData["punch_type"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["punch_type"]);
            modelo.punchDay = rawData["punch_day"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["punch_day"]);
            modelo.punchDate = rawData["punch_date"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["punch_date"]);
            modelo.inDate = rawData["in_date"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["in_date"]);
            modelo.inTime = rawData["in_time"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["in_time"]);
            modelo.inTypeId = rawData["in_type_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["in_type_id"]);
            modelo.outDate = rawData["out_date"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["out_date"]);
            modelo.outTime = rawData["out_time"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["out_time"]);
            modelo.outTypeId = rawData["out_type_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["out_type_id"]);
            modelo.note = rawData["note"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["note"]);
            modelo.benefitTime = rawData["benefit_time"] == DBNull.Value ? 0 : Convert.ToDecimal(rawData["benefit_time"]);
            modelo.inPunchHistory = rawData["in_punch_history"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["in_punch_history"]);
            modelo.outPunchHistory = rawData["out_punch_history"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["out_punch_history"]);
            modelo.editPunchHistory = rawData["edit_punch_history"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["edit_punch_history"]);
            modelo.totalHours = rawData["total_hours"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["total_hours"]);


            /*
            modelo.inPunch = rawData["in_punch"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["in_punch"]);
            modelo.outPunch = rawData["out_punch"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["out_punch"]);
            modelo.editorId = rawData["editor_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["editor_id"]);
            modelo.editorName = rawData["editor_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["editor_name"]);
            modelo.network = rawData["network"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["network"]);
            modelo.date = rawData["date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["date"]);
            modelo.action = rawData["action"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["date"]);
            modelo.lunchOther = rawData["lunch_other"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["lunch_other"]);
            */
            
            
            
            return modelo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        private TimecardModel MapTimeCard(IDataReader rawData)
        {
            TimecardModel modelo = new TimecardModel();
            modelo.departmentId = rawData["department_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["department_id"]);
            modelo.firstName = rawData["first_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["first_name"]);
            modelo.lastName = rawData["last_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["last_name"]);
            modelo.accountTcRequireAdminApproval = rawData["account_tc_require_admin_approval"] == DBNull.Value ? false : Convert.ToBoolean(rawData["account_tc_require_admin_approval"]);
            modelo.accountTcRequireApproval = rawData["account_tc_require_admin_approval"] == DBNull.Value ? false : Convert.ToBoolean(rawData["account_tc_require_approval"]);
            modelo.accountTcRequireEmployeeApproval = rawData["account_tc_require_employee_approval"] == DBNull.Value ? false : Convert.ToBoolean(rawData["account_tc_require_employee_approval"]);
            modelo.accountTcRequireSupervisorApproval = rawData["account_tc_require_supervisor_approval"] == DBNull.Value ? false : Convert.ToBoolean(rawData["account_tc_require_supervisor_approval"]);
            modelo.adminApprovedDate = rawData["admin_approved_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["admin_approved_date"]);
            modelo.approvedAdmin = rawData["approved_admin"] == DBNull.Value ? 0 : Convert.ToInt64(rawData["approved_admin"]);
            modelo.approvedAdminName = Convert.ToString(rawData["approved_admin_name"]);
            modelo.approvedSupervisor = rawData["approved_supervisor"] == DBNull.Value ? 0 : Convert.ToInt64(rawData["approved_supervisor"]);
            modelo.approvedSupervisorName = Convert.ToString(rawData["approved_supervisor_name"]);
            modelo.approvedTz = Convert.ToString(rawData["approved_tz"]);
            modelo.baseRate = rawData["base_rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rawData["base_rate"]);
            modelo.ded = Convert.ToString(rawData["ded"]);
            modelo.deptCode = Convert.ToString(rawData["dept_code"]);
            modelo.deptName = Convert.ToString(rawData["dept_name"]);
            modelo.employeeApproval = rawData["employee_approval"] == DBNull.Value ? short.MinValue : Convert.ToInt16(rawData["employee_approval"]);
            modelo.employeeApprovedDate = rawData["employee_approved_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["employee_approved_date"]);
            modelo.endDate = rawData["end_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["end_date"]);
            modelo.hol = Convert.ToString(rawData["hol"]);
            modelo.isAdminApproved = rawData["is_admin_approved"] == DBNull.Value ? short.MinValue : Convert.ToInt16(rawData["is_admin_approved"]);
            modelo.isEmployeeApproved = rawData["is_employee_approved"] == DBNull.Value ? short.MinValue : Convert.ToInt16(rawData["is_employee_approved"]);
            modelo.isLock = rawData["is_lock"] == DBNull.Value ? short.MinValue : Convert.ToInt16(rawData["is_lock"]);
            modelo.isSupervisorApproved = rawData["is_supervisor_approved"] == DBNull.Value ? short.MinValue : Convert.ToInt16(rawData["is_supervisor_approved"]);
            modelo.ot1 = Convert.ToString(rawData["ot1"]);
            modelo.ot2 = Convert.ToString(rawData["ot2"]);
            modelo.oth = Convert.ToString(rawData["oth"]);
            modelo.payrollNumber = Convert.ToString(rawData["payroll_number"]);
            modelo.periodTypeId = rawData["period_type_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["period_type_id"]);
            modelo.reg = Convert.ToString(rawData["reg"]);
            modelo.sic = Convert.ToString(rawData["sic"]);
            modelo.startDate = rawData["start_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["start_date"]);
            modelo.supervisorApproval = Convert.ToBoolean(rawData["supervisor_approval"]);
            modelo.supervisorApprovedDate = rawData["supervisor_approved_date"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["supervisor_approved_date"]);
            modelo.timeCard = Convert.ToString(rawData["timeCard"]);
            modelo.timecardApproval = Convert.ToBoolean(rawData["timecard_approval"]);
            modelo.timecardId = rawData["timecard_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["timecard_id"]);
            modelo.tot = Convert.ToString(rawData["tot"]);
            modelo.userId = rawData["id"] == DBNull.Value ? 0 : Convert.ToInt64(rawData["id"]);
            modelo.vac = Convert.ToString(rawData["vac"]);

            return modelo;
        }



        private UserModel MapUsersSearch(IDataReader rawData)
        {
            UserModel user = new UserModel();
            user.accountId = rawData["account_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["account_id"]);
            user.id = rawData["id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["id"]);
            user.roleId = rawData["role_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["role_id"]);
            user.roleName = rawData["role_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["role_name"]);
            user.departmentId = rawData["department_id"] == DBNull.Value ? 0 : Convert.ToInt32(rawData["department_id"]);
            user.firstName = rawData["first_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["id"]);
            user.lastName = rawData["last_name"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["last_name"]);
            user.lastVisit = rawData["last_visit"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rawData["last_visit"]);
            user.lastIp = rawData["last_ip"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["last_ip"]);
            user.departmentCode = rawData["CODE"] == DBNull.Value ? string.Empty : Convert.ToString(rawData["CODE"]);

            return user;
        }
        #endregion Maps
    }
}
