import R from 'ramda';

export class State {
  constructor() {
    this.state= {};
    // this info should come with login info
    this.state.user = {
      id: 3328,
      firstName: 'TheLegend',
      lastName: '27',
      accountId: 1001,
      departments: [ 
        {id:1357, name: '0600-1400', code: '6-14'},
        {id:1356, name: '@TE&amhp\'ST', code: 'TEST2'},
        {id:1353, name: '@Test Department & Trans', code: 'TST'},
        {id:1358, name: 'AES Test', code: 'AES'},
        {id:1352, name: 'Quality & Control', code: 'QC'},
        {id:1355, name: 'Test-ZK', code: '5'},
        {id:1354, name: 'Testing', code: 'TEST'}
      ]
    };
    this.state.periodId = 0;
  }

  get() {
    return R.clone(this.state);
  }

  set(type, data) {
    this.state[type] = data;
  }
}

