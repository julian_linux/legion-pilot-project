import template from './modal.html';
import controller from './modal.controller';

export default {  
  template,
  controller,
  replace: true,
  bindings: {
    card: '<',
  },
  require: {
    parent: '^^timeCards'
  }
};
