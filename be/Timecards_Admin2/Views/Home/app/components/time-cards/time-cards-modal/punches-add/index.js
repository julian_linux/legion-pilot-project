import angular from 'angular';

import component from './punches-add.component';

import './punches-add.scss';

export default angular
    .module('app.components.time-cards-modal.punches-add', [])
    .component('timeCardsModalPunchesAdd', component)
    .name;
