import R from 'ramda';
class TimeCardsModalController {
  constructor(ModalService, State, GLOBAL, $timeout){
    this.service = ModalService;
    this.state = State;
    this.global = GLOBAL;
    this.$timeout = $timeout;
  }

  $onInit() {
    const { departments } = this.state.get().user;
    this.departments = departments;
    this.days = [];
    const { selectedWeek, selectedCard, selectedCardIndex, timeCards } = this.state.get();
    this.prevCard = timeCards[selectedCardIndex - 1];
    this.nextCard = timeCards[selectedCardIndex + 1];
    this.card = selectedCard;
    this.selectedWeek = selectedWeek;
    this.configureDaysOfWeek();
  }

  updateCard(next) {
    const { selectedCard, timeCards } = this.state.get();
    let { selectedCardIndex } = this.state.get();
    if(next){
      selectedCardIndex ++;
      this.card = this.nextCard;
      this.prevCard = selectedCard;
      this.nextCard = timeCards[selectedCardIndex + 1];
    }else {
      selectedCardIndex --;
      this.card = this.prevCard;
      this.prevCard = timeCards[selectedCardIndex - 1];
      this.nextCard = selectedCard;
    }
    this.state.set('selectedCard', this.card);
    this.state.set('selectedCardIndex', selectedCardIndex);
    this.configureDaysOfWeek();
  }

  closeModal(e) {
    e.preventDefault();
    this.parent.showModal = false;
  }

  configureDaysOfWeek() {
    const startDate = this.selectedWeek.startDate;
    this.days=[];
    for( let i = 0; i < 7; i++){
      const date = startDate.clone().add(i, 'days');
      const data = {
        punches: [], // extract data from state
        date:{
          date,
          formatted: `${R.toUpper(date.format('ddd'))} ${date.format('MM/DD/YY')}`,
        }
      };

      this.days.push(data);
    }
    this.getPunchesByDate();
  }

  getPunchesByDate(day = 0, onlyThisDay = false) {
    const date = this.selectedWeek.startDate.clone().add(day, 'days').format('YYYY-MM-DD');
    this.service.getPunchesByDate(date).then(punches => {
      this.days[day].punches = this.processPunchesData(punches);
      if(onlyThisDay) {
        return;
      }
      day++;
      if (day < 7){
        this.getPunchesByDate(day);
      }
    });
  }

  showAddPunchForm(indexDay, day) {
    day.showForm = true;
    // $("#liPunchForm-"+indexDay).insertAfter($(`#table-punches-${indexDay} li:last`));
    // push the form in the end
  }

  processPunchesData(data) {
    const { user } = this.state.get();
    const { payCodes } = this.global;
    const res = [];
    for (let i= 0; i < data.length; i++) {
      data[i].hideRow = false;
      if (data[i].paycodeId === 1) {

        data[i].departmentName = R.filter(R.propEq('id', data[i].departmentId), user.departments)[0].name;
        data[i].inPunchTime = this.formatTime(data[i].inTime);
        data[i].outPunchTime = this.formatTime(data[i].outTime);

      } else if (R.contains(data[i].paycodeId, [2, 3, 4, 5]) ) {

        const benefit = R.filter(R.propEq('id', data[i].paycodeId), payCodes)[0];
        data[i].benefitType = benefit.name;
        data[i][R.toLower(benefit.code)]=data[i].benefitTime;

      } else if (R.contains(data[i].paycodeId, [6, 7]) ) {

        data[i].breakOrLunch = R.filter(R.propEq('id', data[i].paycodeId), payCodes)[0].name;
        data[i].reg=data[i].benefitTime;

      }
    }

    return data;
  }

  formatTime(time) {
    const t = time.split(':');
    let newHour = t[0], newString = 'AM';
    if(t[0] > 12) { newHour = parseInt(t[0], 10); newString = 'PM';}
    return `${newHour}:${t[1]} ${newString}`;
  }

  closePunchForm(indexDay, indexPunch) {
    this.days[indexDay].showForm = false;
    const punches = this.days[indexDay].punches;
    for(let i = 0; i < punches.length; i++) {
      this.days[indexDay].punches[i].hideRow = false;
    }
    
  }

  editPunch(e, indexPunch, indexDay, punch, day) {
    this.closePunchForm(indexDay, indexPunch);
    const elem = e.currentTarget;
    const punches = this.days[indexDay].punches;
    for(let i = indexPunch; i < punches.length; i++) {
      this.days[indexDay].punches[i].hideRow = true;
    }
    this.$timeout(() => {
      day.showForm = true;
      this.indexPunch = indexPunch;
    }, 50);
  }

  deletePunch(index) {
    
  }


}

TimeCardsModalController.$inject = ['ModalService', 'State', 'GLOBAL', '$timeout'];

export default TimeCardsModalController;