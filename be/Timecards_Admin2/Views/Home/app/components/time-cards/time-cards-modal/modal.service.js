import M from 'moment';
import R from 'ramda';

class ModalService {
  constructor($http, GLOBAL, State) {
    this.$http = $http;
    this.global = GLOBAL;
    this.requestParams = GLOBAL.requestParams;
    this.state = State;
    this.punchId =1001;
  }

  savePunch(punch) {
    const req = R.clone(this.requestParams);
    // delete req.params;
    // req.data = JSON.stringify(punch);
    // req.method = 'POST';
    // delete req.headers;
    req.url += 'AddPunch';
    // this.$http.defaults.headers.post["Content-Type"] = "application/json";
    // if (true) { 
    //   return Promise.resolve(true);
    // }
    return this.$http.post(req.url, JSON.stringify(punch)).then(response => true);
  }

  getPunchesByDate(date) {
    const { user } = this.state.get();
    const { selectedCard } = this.state.get();
    const req = R.clone(this.requestParams);
    delete req.data;
    req.method = 'GET';
    req.params.accountId = selectedCard.accountId;
    req.params.userId = selectedCard.userId;
    req.params.date = date;
    req.url += 'GetPunchesByDate';
    return this.$http(req).then(response => response.data );
  }

  deletePunch(punch) {
    const req = R.clone(this.requestParams);
    req.url += 'DeletePunch';
    const { selectedCard } = this.state.get();
    punch.accountId = selectedCard.accountId;
    // this.$http.defaults.headers.post["Content-Type"] = "application/json";
    // if (true) { 
    //   return Promise.resolve(true);
    // }
    return this.$http.post(req.url, JSON.stringify(punch)).then(response => true);
  }
}

ModalService.$inject = ['$http', 'GLOBAL', 'State'];

export default ModalService;