import template from './punches-add.html';
import controller from './punches-add.controller';

export default {  
  template,
  controller,
  replace: true,
  bindings: {
    index: '<'
  },
  require: {
    parent: '^timeCardsModal'
  }
};
