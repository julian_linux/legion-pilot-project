import M from 'moment';
import R from 'ramda';

class TimeCardsService {
  constructor($http, GLOBAL, State) {
    this.$http = $http;
    this.global = GLOBAL;
    this.state = State;
    this.requestParams = GLOBAL.requestParams;
  }

  getTimeCards() {
    const req = R.clone(this.requestParams);
    delete req.data;

    const { user, periodId } = this.state.get();

    req.method = 'GET';
    req.params.accountId = user.accountId;
    req.params.periodId = periodId;
    req.url += 'GetTimeCards';

    return this.$http(req).then(response => {
       const data = this.processData(response.data);
       this.state.set('timeCards', data);
       return data;
    });
  }

  getTimeCardInfo() {
    const req = R.clone(this.requestParams);
    delete req.data;

    const { user, selectedCard, periodId } = this.state.get();

    req.method = 'GET';
    req.params.accountId = user.accountId;
    req.params.userId = selectedCard.userId;
    req.params.periodId = periodId;
    req.url += 'GetTimeCards';

    return this.$http(req).then(response => this.processData(response.data));
  }

  processData(data) {
    for(let i=0; i< data.length; i++){
      data[i].startDate = M(data[i].startDate).format('MM/DD/YY');
      data[i].endDate = M(data[i].endDate).format('MM/DD/YY');
    }
    this.state.set('timeCards', data);
    return data;
  }

}

TimeCardsService.$inject = ['$http', 'GLOBAL', 'State'];

export default TimeCardsService;