class TimeCardController {
  constructor(State, TimecardsService){
    'ngInject'
    this.state = State;
    this.service = TimecardsService;
  }

  selectCard() {
    this.state.set('selectedCard', this.card);
    this.state.set('selectedCardIndex', this.cardIndex);
    this.service.getTimeCardInfo().then(timeCard => {
      this.parent.showModal = true;
    });
  }
}

TimeCardController.$inject = ['State', 'TimeCardsService'];

export default TimeCardController;