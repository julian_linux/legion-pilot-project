import template from './approval.html';
import controller from './approval.controller';

export default {  
  template,
  controller,
  replace: true,
  bindings: {
    card: '<'
  }
};
