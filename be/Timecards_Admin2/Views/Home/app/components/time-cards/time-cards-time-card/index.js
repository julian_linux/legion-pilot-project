import angular from 'angular';

import component from './time-card.component';

import approval from './approval';

export default angular
    .module('app.components.time-card', [
        approval, 
    ])
    .component('timeCard', component)
    .name;
