class TimeCardsController{
  constructor(TimeCardsService) {
    'ngInject'
    this.timeCards = [];
    this.showModal = false;
    this.service = TimeCardsService;
  }

  $onInit() {
    this.service.getTimeCards().then(timeCards => this.timeCards = timeCards);
  }

  openModal() {
    $("#time-cards-view-modal").openModal();
  }
}

TimeCardsController.$inject = ['TimeCardsService'];

export default TimeCardsController;