import moment from 'moment';
import R from 'ramda';

class PayPeriodController {
  constructor(State){
    this.state = State;
  }
  $onInit(){
    this.showTable = false;
    this.weeks = [];
    this.years = [];
    this.actualYear=null;
    this.plusWeeks = 0;
    this.plusYears = 0;
    this.actualYear = moment().format('YYYY')
    this.calculateYears();
  }

  changeShowTable(e) {
    e.preventDefault();
    this.showTable=!this.showTable;
  }

  calculateYears() {
    const m =  moment().year(this.actualYear);
    this.years = [];
    let cont = 0;
    for (let i = (this.plusYears - 3); i < (this.plusYears + 4); i++) {
      const year = R.sum([this.actualYear, i]);
      this.years.push(year);
    }
    this.calculateWeeks();
  }

  calculateWeeks() {
    const m =  moment().year(this.actualYear);
    this.weeks = [];
    let cont = 0;
    for (let i = (this.plusWeeks - 3); i < (this.plusWeeks + 4); i++) {
      let week = {
        startDate: m.clone().add(i, 'weeks').isoWeekday(0).format('MM/DD/YY'),
        endDate: m.clone().add(i, 'weeks').isoWeekday(6).format('MM/DD/YY')
      };
      this.weeks.push(week);
    }
    const selectedWeek = {
        startDate: m.clone().isoWeekday(0),
        endDate: m.clone().isoWeekday(6)
    }
    this.state.set('selectedWeek', selectedWeek);
  }

  moveWeeks(direction) {
    if(direction=='up'){
      this.plusWeeks++;
    }else{
      this.plusWeeks--;
    }
    this.calculateWeeks();
  }

  moveYears(direction) {
    if(direction=='up'){
      this.plusYears++;
    }else{
      this.plusYears--;
    }
    this.calculateYears();
  }

  updateWeek(index) {
    this.plusWeeks += index - 3;
    this.showTable = false;
    this.calculateWeeks();
  }

  updateYear(year) {
    this.actualYear=year;
    this.calculateYears();
  }
}

PayPeriodController.$inject = ['State'];

export default PayPeriodController;