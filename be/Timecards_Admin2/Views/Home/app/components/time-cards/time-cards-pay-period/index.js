import angular from 'angular';

import component from './pay-period.component';

import './pay-period.scss';

export default angular
    .module('app.components.time-cards.pay-period', [])
    .component('payPeriod', component)
    // .config(config)
    .name;
