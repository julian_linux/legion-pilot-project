export default class TimeCard {
    constructor() {
      this.accountId;
      this.userId;
      this.departmentId;
      this.deptCode = "";
      this.deptName = "";
      this.employeeApproval;
      this.startDate = "";
      this.endDate = "";
      this.firstName = "";
      this.lastName = "";
      this.periodTypeId;
      this.timecardId;
      this.baseRate;
      this.payrollNumber = "";
      this.timeCard = "";
      this.timecardApproval;
      this.isAdminApproved;
      this.isSupervisorApproved;
      this.isLock;
      this.adminApprovedDate = "";
      this.supervisorApproval;
      this.accountTcRequireApproval;
      this.accountTcRequireAdminApproval;
      this.accountTcRequireSupervisorApproval;
      this.accountTcRequireEmployeeApproval;
      this.supervisorApprovedDate = "";
      this.approvedAdmin;
      this.approvedAdminName = "";
      this.approvedTz;
      this.approvedSupervisor;
      this.approvedSupervisorName = "";
      this.isEmployeeApproved;
      this.employeeApprovedDate = "";
      this.reg = "";
      this.ot1 = "";
      this.ot2 = "";
      this.vac = "";
      this.hol = "";
      this.sic = "";
      this.oth = "";
      this.tot = "";
      this.ded = "";
      this.isTimecardViewable;
      this.timeZoneId;
      this.periodId;
      this.minuteFormatId;
      this.adminId
    }
}