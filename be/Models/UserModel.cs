﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Models
{
    [Serializable]
    [DataContract]
    public class UserModel
    {
        [DataMember]
        public Int64 id { get; set; }
        [DataMember]
        public Int32 accountId { get; set; }
        [DataMember]
        public Int64 masterUserId { get; set; }
        [DataMember]
        public int roleId { get; set; }
        [DataMember]
        public string roleName { get; set; }
        [DataMember]
        public int rightId { get; set; }
        [DataMember]
        public Int64 departmentId { get; set; }
        [DataMember]
        public string departmentCode { get; set; }
        [DataMember]
        public int timeZoneId { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public string phone { get; set; }
        [DataMember]
        public decimal baseRate { get; set; }
        [DataMember]
        public int weeklyRule { get; set; }
        [DataMember]
        public string payrollNumber { get; set; }
        [DataMember]
        public string pinNumber { get; set; }
        [DataMember]
        public string notice { get; set; }
        [DataMember]
        public string notes { get; set; }
        [DataMember]
        public DateTime lastVisit { get; set; }
        [DataMember]
        public string lastIp { get; set; }
        [DataMember]
        public int visitCount { get; set; }
        [DataMember]
        public DateTime lastPunch { get; set; }
        [DataMember]
        public int lastPunchType { get; set; }
        [DataMember]
        public DateTime vacationStartDate { get; set; }
        [DataMember]
        public decimal vacationStartBalance { get; set; }
        [DataMember]
        public DateTime sickStartDate { get; set; }
        [DataMember]
        public bool isTimeCardViewable { get; set; }
        [DataMember]
        public int timeCardAccess { get; set; }

    }
}
