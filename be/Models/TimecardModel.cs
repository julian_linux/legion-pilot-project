﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    [DataContract]
    public class TimecardModel
    {
        [DataMember]
        public Int64 accountId { get; set; }
        [DataMember]
        public long userId { get; set; }
        [DataMember]
        public Int64 departmentId { get; set; }
        [DataMember]
        public string deptCode { get; set; }
        [DataMember]
        public string deptName { get; set; }
        [DataMember]
        public Int16 employeeApproval { get; set; }
        [DataMember]
        public DateTime startDate { get; set; }
        [DataMember]
        public DateTime endDate { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public int periodTypeId { get; set; }
        [DataMember]
        public int timecardId { get; set; }
        [DataMember]
        public decimal baseRate { get; set; }
        [DataMember]
        public string payrollNumber { get; set; }
        [DataMember]
        public string timeCard { get; set; }
        [DataMember]
        public bool timecardApproval { get; set; }
        [DataMember]
        public Int16 isAdminApproved { get; set; }
        [DataMember]
        public Int16 isSupervisorApproved { get; set; }
        [DataMember]
        public Int16 isLock { get; set; }
        [DataMember]
        public DateTime adminApprovedDate { get; set; }
        [DataMember]
        public bool supervisorApproval { get; set; }
        [DataMember]
        public bool accountTcRequireApproval { get; set; }
        [DataMember]
        public bool accountTcRequireAdminApproval { get; set; }
        [DataMember]
        public bool accountTcRequireSupervisorApproval { get; set; }
        [DataMember]
        public bool accountTcRequireEmployeeApproval { get; set; }
        [DataMember]
        public DateTime supervisorApprovedDate { get; set; }
        [DataMember]
        public Int64 approvedAdmin { get; set; }
        [DataMember]
        public string approvedAdminName { get; set; }
        [DataMember]
        public string approvedTz { get; set; }
        [DataMember]
        public Int64 approvedSupervisor { get; set; }
        [DataMember]
        public string approvedSupervisorName { get; set; }
        [DataMember]
        public Int16 isEmployeeApproved { get; set; }
        [DataMember]
        public DateTime employeeApprovedDate { get; set; }
        [DataMember]
        public string reg { get; set; }
        [DataMember]
        public string ot1 { get; set; }
        [DataMember]
        public string ot2 { get; set; }
        [DataMember]
        public string vac { get; set; }
        [DataMember]
        public string hol { get; set; }
        [DataMember]
        public string sic { get; set; }
        [DataMember]
        public string oth { get; set; }
        [DataMember]
        public string tot { get; set; }
        [DataMember]
        public string ded { get; set; }
        [DataMember]
        public bool isTimecardViewable { get; set; }
        [DataMember]
        public int timeZoneId { get; set; }
        [DataMember]
        public int periodId { get; set; }
        [DataMember]
        public int minuteFormatId { get; set; }
        [DataMember]
        public int adminId { get; set; }
    }
}
