﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class SummaryModel
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public int deparmentId { get; set; }
        public string deparmentName { get; set; }
        public string departmentCode { get; set; }
        public string reg { get; set; }
        public string ot1 { get; set; }
        public string ot2 { get; set; }
        public string vac { get; set; }
        public string hol { get; set; }
        public string sic { get; set; }
        public string oth { get; set; }
        public string tot { get; set; }
        public string regp { get; set; }
        public string dedp { get; set; }
        public string ot1p { get; set; }
        public string ot2p { get; set; }
        public string vacp { get; set; }
        public string holp { get; set; }
        public string sicp { get; set; }
        public string othp { get; set; }
        public string totp { get; set; }

    }
}
