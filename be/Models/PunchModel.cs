﻿using System;
using System.Runtime.Serialization;

namespace Models
{
    [Serializable]
    [DataContract]
    public class PunchModel
    {
        [DataMember]
        public Int32 id { get; set; }
        [DataMember]
        public string accountId { get; set; }
        [DataMember]
        public Int32 userId { get; set; }
        [DataMember]
        public Int32 departmentId { get; set; }
        [DataMember]
        public string departmentCode { get; set; }
        [DataMember]
        public Int32 paycodeId { get; set; }
        [DataMember]
        public DateTime inPunch { get; set; }
        [DataMember]
        public DateTime outPunch { get; set; }
        [DataMember]
        public Decimal benefitTime { get; set; }
        [DataMember]
        public Int32 editorId { get; set; }
        [DataMember]
        public string editorName { get; set; }
        [DataMember]
        public string note { get; set; }
        [DataMember]
        public string network { get; set; }
        [DataMember]
        public DateTime date { get; set; }
        [DataMember]
        public Int32 action { get; set; }
        [DataMember]
        public Int32 lunchOther { get; set; }

        [DataMember]
        public string punchType { get; set; }
        [DataMember]
        public string punchDay { get; set; }
        [DataMember]
        public string punchDate { get; set; }
        [DataMember]
        public string inTime { get; set; }
        [DataMember]
        public string inDate { get; set; }
        [DataMember]
        public Int32 inTypeId { get; set; }
        [DataMember]
        public string outTime { get; set; }
        [DataMember]
        public string outDate { get; set; }
        [DataMember]
        public Int32 outTypeId { get; set; }
        [DataMember]
        public string inPunchHistory { get; set; }
        [DataMember]
        public string outPunchHistory { get; set; }
        [DataMember]
        public string editPunchHistory { get; set; }
        [DataMember]
        public string totalHours { get; set; }
    }
}
