﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    [DataContract]
    public class Request
    {
        [DataMember]
        public UserModel user { get; set; }
        [DataMember]
        public string keyword { get; set; }
    }
}
