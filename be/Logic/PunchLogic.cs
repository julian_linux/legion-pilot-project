﻿using System;
using Models;
using DataAccess;
using System.Collections.Generic;

namespace Logic
{
    public class PunchLogic
    {
        public Int64 AddPunch(PunchModel pPunch)
        {
            TimecardData data = new TimecardData();
            return data.AddPunch(pPunch);
        }

        public void DeletePunch(PunchModel pPunch)
        {
            TimecardData data = new TimecardData();
            data.DeletePunch(pPunch);
        }

        public IList<PunchModel> GetPunchesByDate(PunchModel pPunch)
        {
            TimecardData data = new TimecardData();
            return data.GetPunchesByDate(pPunch);
        }
    }
}
