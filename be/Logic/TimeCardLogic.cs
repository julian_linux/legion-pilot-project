﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Models;

namespace Logic
{
    public class TimeCardLogic
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IList<TimecardModel> GetTimecardsPayroll(TimecardModel pTimecard)
        {
            TimecardData data = new TimecardData();
            return data.GetTimecardsPayroll(pTimecard);
        } 

        public void EditTimeCard(TimecardModel pTimecard)
        {
            TimecardData data = new TimecardData();
            data.EditTimecard(pTimecard);
        }

        /// <summary>
        /// Return hours by deparment
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public IList<SummaryModel> GetHoursByDepartment(int accountId)
        {
            TimecardData data = new TimecardData();
            return data.GetHoursByDepartment(accountId);
        }


        /// <summary>
        /// Get Summaries
        /// </summary>
        /// <param name="pTimecard"></param>
        /// <returns></returns>
        public IList<SummaryModel> GetSummaries(TimecardModel pTimecard)
        {
            TimecardData data = new TimecardData();
            return data.GetSummaries(pTimecard);
        }
    }
}
