﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Models;

namespace Logic
{
    public class UserLogic
    {
        public IList<UserModel> GetEmployees()
        {
            IList<UserModel> ret = new List<UserModel>();
            TimecardData data = new TimecardData();
            ret = data.GetEmployees();
            return ret;
        }

        public IList<UserModel> SearchEmployees(UserModel pUser, string keyword)
        {
            IList<UserModel> user = new List<UserModel>();
            TimecardData data = new TimecardData();
            user = data.SearchEmployees(pUser, keyword);
            return user;
        }

        public UserModel GetUser(UserModel pUser)
        {
            UserModel user = new UserModel();
            TimecardData data = new TimecardData();
            user = data.GetUser(pUser);
            return user;
        }
    }
}
