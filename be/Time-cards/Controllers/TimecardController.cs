﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Models;
using Logic;

namespace Time_cards.Controllers
{
    public class TimecardController : ApiController
    {
        [HttpGet]
        [Route("GetTimeCards")]
        public IList<TimecardModel> GetTimeCards([FromUri] TimecardModel timecard)
        {
            System.Diagnostics.Debug.WriteLine(timecard);
            TimeCardLogic logic = new TimeCardLogic();
            return logic.GetTimecardsPayroll(timecard);
        }

        [HttpPost]
        [Route("EditTimeCard")]
        public void EditTimeCard([FromBody] TimecardModel timecard)
        {
            TimeCardLogic logic = new TimeCardLogic();
            logic.EditTimeCard(timecard);
        }

        [HttpGet]
        [Route("GetHoursByDeparment")]
        public IList<SummaryModel> GetHoursByDepartment([FromBody] int accountId)
        {
            TimeCardLogic logic = new TimeCardLogic();
            return logic.GetHoursByDepartment(accountId);
        }


        [HttpGet]
        [Route("GetSummaries")]
        public IList<SummaryModel> GetSummaries([FromBody] TimecardModel sol)
        {
            TimeCardLogic logic = new TimeCardLogic();
            return logic.GetSummaries(sol);
        }
    }
}
