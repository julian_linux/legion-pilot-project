﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Models;
using Logic;
using Newtonsoft.Json.Linq;

namespace Time_cards.Controllers
{
    public class UserController : ApiController
    {
        [HttpGet]
        [Route("GetEmployees")]
        public IList<UserModel> GetEmployees()
        {
            IList<UserModel> employees = new List<UserModel>();
            UserLogic logic = new UserLogic();
            employees = logic.GetEmployees();
            return employees;
        }

        [HttpGet]
        [Route("SearchEmployees")]
        public IList<UserModel> SearchEmployees([FromBody]UserModel pUser, string keyword)
        {
            IList<UserModel> employees = new List<UserModel>();
            UserLogic logic = new UserLogic();            

            employees = logic.SearchEmployees(pUser, keyword);
            return employees;
        }

        [HttpGet]
        [Route("GetUser")]
        public UserModel GetUser([FromBody]UserModel pUser)
        {            
            UserLogic logic = new UserLogic();
            return logic.GetUser(pUser);
        }
    }
}
