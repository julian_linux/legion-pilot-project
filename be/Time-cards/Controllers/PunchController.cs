﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Models;
using Logic;

namespace Time_cards.Controllers
{
    public class PunchController : ApiController
    {
        [HttpPost]
        [Route("AddPunch")]
        public void AddPunch([FromBody]PunchModel pPunch)
        {
            //int req = Convert.ToInt16(form["accountId"]);
            System.Diagnostics.Debug.WriteLine("pPunch");
            System.Diagnostics.Debug.WriteLine(pPunch);
            Logic.PunchLogic logic = new Logic.PunchLogic();
           // return null;//logic.AddPunch(pPunch);
        }

        [HttpPost]
        [Route("DeletePunch")]
        public void DeletePunch([FromBody]PunchModel pPunch)
        {
            Logic.PunchLogic logic = new Logic.PunchLogic();
            logic.DeletePunch(pPunch);
        }

        [HttpGet]
        [Route("GetPunchesByDate")]
        public IList<PunchModel> GetPunchesByDate([FromUri] PunchModel punch)
        {
            PunchLogic logic = new PunchLogic();
            return logic.GetPunchesByDate(punch);
        }
    }
}
