﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net;
using System.IO;
using Models;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;

namespace UserInterface.Controllers
{
    public class UsuariosController : Controller
    {
        // GET: Usuarios
        public  ActionResult Index()
        {
            UserModel modelo = new UserModel();
            modelo.accountId = 10000;
            modelo.id = 1;

            string url = "http://localhost:16261/GetUser/";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);

                // Configurar encabezados para que la petición de realice en formato JSON
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                MemoryStream stream1 = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(UserModel));
                ser.WriteObject(stream1, modelo);
                
                //HttpResponseMessage response = await client.PostAsync(url,ser);

            }

            var webrequest = (HttpWebRequest)System.Net.WebRequest.Create(url);
            using (var response = webrequest.GetResponse())
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                var result = reader.ReadToEnd();
                ViewBag.respuesta = result;
            }     
            return View();
        }
    }
}